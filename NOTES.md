# Developer notes

<!-- TOC -->
- [Setting up the environment](#setting-up-the-environment)
- [Setting up the project](#setting-up-the-project)
- [Gathering the data](#gathering-the-data)
- [Structuring the data](#structuring-the-data)
	- [Rationale behind my choices](#rationale-behind-my-choices)
<!-- /TOC -->


## Setting up the environment

First, as a Python amateur I set out to create a project structure following the best practice,
previously I had just messed around with Python for myself so I just relied on the distribution provided package
and at most activated virtual environments manually.
Initially I didn't know where to look, as most sources that came up at the top were giving some generic guidance,
but as I saw the vagueness that those suggestions presented I decided to look for a more solid alternative.

I remembered my experience NPM/PNPM (JavaScript) and very the brief, but pleasant, one with with Cargo (Rust) where both would allow one
to create a new project with a standardized structure, and indeed it was by looking for the "Python Cargo" that
I stumbled across [this thread](https://www.reddit.com/r/rust/comments/sze8nn/pip_and_cargo_are_not_the_same/)
where Poetry was mentioned the best, albeit not up to par, package manager for Python.

So I started reading the [website](https://python-poetry.org/) to see how to set it up and I was glad to find out that the installation was independent
from the OS's packages, as are [Volta](https://volta.sh/) (to get NodeJS) and [rustup](https://rustup.rs/),
then I got to creating the first project (which is this) and I started searching the internet messily to
understand how to do things, just after I had discovered what I needed for my terminal/VSCodium/Browser workflow,
I find a nifty little [website](https://www.pythoncheatsheet.org/blog/python-projects-with-poetry-and-vscode-part-1)
that lays down the information very nicely, wish I found it sooner, but oh well...

## Setting up the project

Working with Poetry is a breeze, dependency management is easy and, as I wished,  
I could also divide them into project dependencies and development dependencies.  
Then I searched around for the usual good stuff that a "serious" project needs:

- Linter
- Formatter (wish it was integrated in the former, like ESLint)
- Testing suite 
	(admittedly still never used one in any of my projects 
	and I'll skip it for now too,  
	as it would only increase the workload, which I can't afford with this tight deadline)

So this is what I settled on:
| Tool          |   Name |
| :-------------- | -------: |
| Linter        |   [Ruff](https://astral.sh/ruff) |
| Formatter     |  [Black](https://black.readthedocs.io/en/stable/) |

## Gathering the data
The website where the data is hosted is fairly well documented so it might not be too difficult to make sense of it.

## Structuring the data
Needing to use Microsoft SQL Server I modeled the entities as follows:

- `postcode`, doesn't depend on anything:
	```sql
	CREATE TABLE postcode (
		id int PRIMARY KEY,
		code nvarchar(20) NOT NULL,
		CONSTRAINT u_code UNIQUE(code)
	);
  ```
- `property`, doesn't depend on anything:
	```sql
	CREATE TABLE property (
		id int PRIMARY KEY IDENTITY(1,1),
		property_type char(1),
		age char(1),
		duration char(1),
		PAON nvarchar(20),
		SAON nvarchar(20),
		street nvarchar(50),
		locality nvarchar(50),
		town nvarchar(50),
		district nvarchar(50),
		county nvarchar(50),
		PPD_type char(1),
		record_status char(1),
		CONSTRAINT u_property UNIQUE(
			county, 
			district, 
			town, 
			locality, 
			street, 
			SAON, 
			PAON
		)
	);
	```
- `propertyTransaction` (`transaction` seems to be a reserved keyword), depends on both `postcode` and `property`:
	```sql
	CREATE TABLE propertyTransaction (
		id char(36) PRIMARY KEY,
		price int,
		transfer_date datetime,
		propertyID int,
		postcodeID int,
		CONSTRAINT FK_propertyTransaction_property FOREIGN KEY (propertyID)
			REFERENCES property (id),
		CONSTRAINT FK_propertyTransaction_postcode FOREIGN KEY (postcodeID)
			REFERENCES postcode (id),
	);
	```

### Rationale behind my choices
I tried identifying the fields that would contain a single character (actually a [single byte](https://learn.microsoft.com/en-us/sql/t-sql/data-types/char-and-varchar-transact-sql?view=sql-server-ver16#arguments), but for all intents and purposes of this application, those two match) and set those as `char(1)` appropriately.  
The rest of the fields that seemed quite variable I made into `nvarchar(N)` and since I didn't have much time to figure out the patterns in the variable fields that would have been good candidates for being used as primary keys, I threw a quick and trusty `id` in there.  
Instead, for the transactions, it looks like the id is always the same length and so, at maybe the cost of performance, I used the string that identifies them, since it was too good an opportunity to use its natural key to pass up