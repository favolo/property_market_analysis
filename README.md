<sup>(Exercise)</sup>

# Property market analysis

<!-- TOC -->
- [How to run](#how-to-run)
- [Develop](#develop)
- [Data](#data)
	- [Transaction format](#transaction-format)
- [Notes](#notes)
	- [Data provider](#data-provider)
<!-- /TOC -->


## How to run

This project uses [Poetry](https://python-poetry.org/) to manage its dependencies and the virtual environment.

Clone the repository:

```
git clone https://codeberg.org/favolo/property_market_analysis
```

Install the dependencies:

```
cd property_market_analysis
poetry install
```

Create a directory named `./data` and download the [CSV files](#data) there,  
then proceed to run:

```
poetry run python property_market_analysis/main.py
```

If you want to run the Jupyter Notebook:
```
jupyter notebook
```
open the URL that you see in the output in your browser and your can start interacting with it

## Develop

If you use VSCodium, you will need to activate Potery's virtual environment and then run the editor from there,  
if you want to have it detect your tools (trick courtesy of [Python Cheatsheet](https://www.pythoncheatsheet.org/blog/python-projects-with-poetry-and-vscode-part-2) once again, coming in clutch):

```
poetry shell
codium .
```

## Data

The data used comes from the United Kingdom's government:

- Data sources:
  - [Latest](https://www.gov.uk/government/statistical-data-sets/price-paid-data-downloads#yearly-file)
  - [Archive](https://data.gov.uk/dataset/314f77b3-e702-4545-8bcb-9ef8262ea0fd/archived-price-paid-data-1995-to-2017)
- [Description](https://www.gov.uk/guidance/about-the-price-paid-data#explanations-of-column-headers-in-the-ppd)

### Transaction format

```
"{03CCA900-D6A0-4F45-8926-D0D0C048452B}",
"125000",
"2013-05-03 00:00",
"DE11 9TR",
"T",
"Y",
"F",
"26",
"",
"SALFORD WAY",
"CHURCH GRESLEY",
"SWADLINCOTE",
"SOUTH DERBYSHIRE",
"DERBYSHIRE",
"A",
"A"
```
<details>
	<summary>
	Specifications
	</summary>

<table>
	<thead>
		<tr>
			<th>Entity</th>
			<th>Field</th>
			<th>Data sample</th>
		</tr>
	</thead>
	<tbody>
		<tr>
			<td rowspan="3"><strong>Transaction</strong></td>
			<td>0:ID</td>
			<td><code>{03CCA900-D6A0-4F45-8926-D0D0C048452B}</code></td>
		</tr>
		<tr>
			<td>1:price</td>
			<td><code>125000</code></td>
		</tr>
		<tr>
			<td>2:transfer_date</td>
			<td><code>2013-05-03 00:00</code></td>
		</tr>
		<tr>
			<td><strong>Postcode</strong></td>
			<td>3:code</td>
			<td><code>DE11 9TR</code></td>
		</tr>
		<tr>
			<td rowspan="12"><strong>Property</strong></td>
			<td>4:property_type</td>
			<td><code>T</code></td>
		</tr>
		<tr>
			<td>5:age</td>
			<td><code>Y</code></td>
		</tr>
		<tr>
			<td>6:duration</td>
			<td><code>F</code></td>
		</tr>
		<tr>
			<td>7:<strong>PAON</strong></td>
			<td><code>26</code></td>
		</tr>
		<tr>
			<td>8:<strong>SAON</strong></td>
			<td><code></code></td>
		</tr>
		<tr>
			<td>9:<strong>street</strong></td>
			<td><code>SALFORD WAY</code></td>
		</tr>
		<tr>
			<td>10:<strong>locality</strong></td>
			<td><code>CHURCH GRESLEY</code></td>
		</tr>
		<tr>
			<td>11:<strong>town</strong></td>
			<td><code>SWADLINCOTE</code></td>
		</tr>
		<tr>
			<td>12:<strong>district</strong></td>
			<td><code>SOUTH DERBYSHIRE</code></td>
		</tr>
		<tr>
			<td>13:<strong>county</strong></td>
			<td><code>DERBYSHIRE</code></td>
		</tr>
		<tr>
			<td>14:PPD_type</td>
			<td><code>A</code></td>
		</tr>
		<tr>
			<td>15:record_status</td>
			<td><code>A</code></td>
		</tr>
	</tbody>
</table>
</details>

## Notes

You can read a report of the development of the project in [NOTES.md](./NOTES.md)

---

### Data provider

Contains HM Land Registry data © Crown copyright and database right 2021. This data is licensed under the [Open Government Licence v3.0](https://www.nationalarchives.gov.uk/doc/open-government-licence/version/3/).
