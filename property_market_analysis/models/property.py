from enum import Enum


class PropertyType(Enum):
    D = "Detached"
    S = "Semi Detached"
    T = "Terraced"
    F = "Flat"
    O = "Other"


class Age(Enum):
    N = "Old"
    Y = "New"


class Duration(Enum):
    F = "Freehold"
    L = "Leasehold"


class PPDType(Enum):
    A = "Standard price"
    B = "Additional price"


class RecordStatus(Enum):
    A = "Addition"
    C = "Change"
    D = "Delete"


class Property:
    """
    Defined uniquely by:

    .. code-block::
        county + district + town + locality + street + SAON + PAON
    """

    def __init__(
        self,
        property_type: str,
        age: str,
        duration: str,
        PAON: str,
        SAON: str,
        street: str,
        locality: str,
        town: str,
        district: str,
        county: str,
        PPD_type: str,
        record_status: str,
    ):
        self.property_type: PropertyType = PropertyType[property_type.upper()]
        self.age: Age = Age[age.upper()]
        self.duration: Duration = Duration[duration.upper()]
        self.PAON = PAON
        self.SAON = SAON
        self.street = street
        self.locality = locality
        self.town = town
        self.district = district
        self.county = county
        self.PPD_type = PPDType[PPD_type.upper()]
        self.record_status = RecordStatus[record_status.upper()]

    def __hash__(self):
        return hash(
            (
                self.county,
                self.district,
                self.town,
                self.locality,
                self.street,
                self.SAON,
                self.PAON,
            )
        )

    def __eq__(self, other):
        if not isinstance(other, type(self)):
            return NotImplemented
        return (
            self.county == other.county
            and self.district == other.district
            and self.town == other.town
            and self.locality == other.locality
            and self.street == other.street
            and self.SAON == other.SAON
            and self.PAON == other.PAON
        )
