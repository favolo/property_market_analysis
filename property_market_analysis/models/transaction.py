from datetime import datetime

from .property import Property
from .postcode import Postcode


class Transaction:
    """
    Bundles all fields together
    """

    def __init__(
        self,
        ID: str,
        price: int,
        transfer_date: datetime,
        property: Property,
        postcode: Postcode,
    ):
        self.ID = ID
        self.price = price
        self.transfer_date = transfer_date
        self.property = property
        self.postcode = postcode
