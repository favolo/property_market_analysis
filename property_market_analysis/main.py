from pathlib import Path
import csv
import signal
from datetime import datetime
from models.transaction import Transaction
from models.postcode import Postcode
from models.property import Property


def handle_exit(signum, frame):
    print()
    exit(1)


def main():
    signal.signal(signal.SIGINT, handle_exit)

    total_transactions: int = 0
    transaction_files = Path("./data/")
    transactions: list[Transaction] = []
    for file in transaction_files.iterdir():
        partial_transactions: int = 0

        input(f'"{file.name}" starts here:\n' "Press [Enter] to process: ")
        with open(file) as transaction_list:
            csv_reader = csv.reader(transaction_list, delimiter=",")
            for transaction_field in csv_reader:
                transactions.append(
                    Transaction(
                        ID=transaction_field[0],
                        price=int(transaction_field[1]),
                        transfer_date=datetime.strptime(
                            transaction_field[2], "%Y-%m-%d %H:%M"
                        ),
                        postcode=Postcode(code=transaction_field[3]),
                        property=Property(
                            property_type=transaction_field[4],
                            age=transaction_field[5],
                            duration=transaction_field[6],
                            PAON=transaction_field[7],
                            SAON=transaction_field[8],
                            street=transaction_field[9],
                            locality=transaction_field[10],
                            town=transaction_field[11],
                            district=transaction_field[12],
                            county=transaction_field[13],
                            PPD_type=transaction_field[14],
                            record_status=transaction_field[15],
                        ),
                    )
                )

                print(f"[{transaction_field[0]}]", end="")
                if partial_transactions % 5 == 0:
                    print()
                partial_transactions += 1

        properties = set(map(lambda transaction: transaction.property, transactions))
        total_transactions += partial_transactions

        print(
            "\n--------------------------------------------------\n"
            f"{len(properties)} unique properties up to now\n"
            f"Read {partial_transactions} new transactions\n"
            f"Current total: {total_transactions}",
            end="\n\n",
        )

    print(f"Processed {total_transactions} lines.")


if __name__ == "__main__":
    main()
